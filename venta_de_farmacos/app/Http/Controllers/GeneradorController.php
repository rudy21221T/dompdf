<?php
// Our Controller 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
// This is important to add here. 
use PDF;

class GeneradorController extends Controller
{
	public function imprimir()
	{
       // This  $data array will be passed to our PDF blade

		$pdf = PDF::loadView('welcome'); 
		return $pdf->stream('prueba.pdf');
	}

	public function index()
	{
		return view('pdf');
	}
}